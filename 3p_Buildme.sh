#!/bin/bash 

#!/bin/bash
CWD=$(pwd)
NPROC=$(nproc)

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i486 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "i486" ]; then
  OSARCH=""
elif [ "$ARCH" = "i686" ]; then
  OSARCH=""
elif [ "$ARCH" = "x86_64" ]; then
  OSARCH="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

autobuild build  --platform linux$OSARCH

autobuild package  --platform linux$OSARCH

BLIB=$(ls | grep wine64)
md5sum $BLIB >> $BLIB-md5sum.txt

echo $BLIB add this to your autobuild.xml 

sleep 5
