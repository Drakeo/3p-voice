#!/bin/bash

# turn on verbose debugging output for teamcity logs.
set -x

# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then
    echo 'AUTOBUILD not set' 1>&2
    exit 1
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

# This version helps define which archive will be used to extract files from
# Should match the viewer from which the plugins and launcher were extracted from
SLVOICE_VERSION=3.2.0002.10426
SLWINE64_LINUX_VERSION=wine64-4.10.0000.32327

top="$(pwd)"
stage="$top"
openal32="$top/packages"





case "$AUTOBUILD_PLATFORM" in
"windows")
    python -c "import zipfile; zipfile.ZipFile('../slvoice-${SLPLUGINS_VERSION}-Release-win32.zip').extractall()"
;;
"darwin")
    tar -xvf ../slvoice-${SLVOICE_VERSION}-Release-darwin.tar.bz2
;;
"linux64")
  wget -c https://bitbucket.org/Drakeo/3p-voice/downloads/slvoice-3.2.0002.10426-Release-linux.tar.bz2
    tar -xvf slvoice-${SLVOICE_VERSION}-Release-linux.tar.bz2
   mkdir -p $stage/packages/lib/release
   cp -aR  lib/release/*  $stage/packages/lib/release
  
 mkdir -p "$openal32"
 cd "$openal32"
 wget -c https://bitbucket.org/Drakeo/3p-voice/downloads/openal-1.15.1-1.1.0.201505071449-linux-201505071449.tar.bz2
   tar -xvf openal-1.15.1-1.1.0.201505071449-linux-201505071449.tar.bz2
    mkdir -p "$stage/lib/release/32compat"
  cp -aR  $stage/packages/lib/release/*   $stage/lib/release/32compat
 wget -c http://automated-builds-secondlife-com.s3.amazonaws.com/ct2/55967/524409/slvoice-4.10.0000.32327.5fc3fe7c.539691-windows64-539691.tar.bz2
  tar -xvf  slvoice-4.10.0000.32327.5fc3fe7c.539691-windows64-539691.tar.bz2
  mkdir -p "$stage/lib/release/win64"
  cp -aR  $stage/packages/lib/release/DbgHelp.dll  $stage/lib/release/win64
  cp -aR  $stage/packages/lib/release/ortp_x64.dll  $stage/lib/release/win64
  cp -aR  $stage/packages/lib/release/ortp_x64.pdb  $stage/lib/release/win64
  cp -aR  $stage/packages/lib/release/vivoxsdk_x64.dll  $stage/lib/release/win64
  cp -aR  $stage/packages/lib/release/vivoxsdk_x64.lib  $stage/lib/release/win64
  cp -aR  $stage/packages/lib/release/vivoxsdk_x64.pdb  $stage/lib/release/win64
  cp -aR  $stage/packages/bin/release/SLVoice.exe  $stage/lib/release/win64
;;
esac
cd "$top"
# Create the version number file from variable in this file
version=${SLWINE64_LINUX_VERSION}
build=${AUTOBUILD_BUILD_ID:=0}
echo "${version}.${build}" > "${stage}/VERSION.txt"

# Copy over other files
mkdir -p "$stage/LICENSES"
cp "$top/../slvoice.txt" "$stage/LICENSES"

